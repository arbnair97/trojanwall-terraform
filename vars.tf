variable "AWS_REGION" {
  type = string
  default = "ap-south-1"
}

variable "access_key_id" {
  type = string
}

variable "secret_key_id" {
  type = string
}

variable "ansible_engine_eip_allct_id" {
  type = string
}
